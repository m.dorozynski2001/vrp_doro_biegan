## Jak korzystać z Gita

### Struktura kodu

Głógwna część repozytorium nazywa się `main`. Na nim nie robimy żadnych zmian. Zmiany do kodu głównego wykonujemy tylko na branchach. Robimy tak, żeby ułatwić analizę zmian kodu, zapobiegać przepływowi wadliwego kodu do głównej części. W małych apkach nie jest to potrzebne ale przy cało semestralnym projekcie kod może się znacznie rozrosnąć.

### Co to są issues?

Issues jest to struktura w gicie, w której możemy zamieszczać zadania do wykonania i ich opisy. Issue może być otwarte i zamknięte (wykonane). Dzięki temu, po czasie rozwoju repo w łatwy sposób możemy przejrzeć nasze postępy i przypomnieć sobie co kiedy robiliśmy.

### Co to jest merge request?

Merge request tworzony jest dla każdego issue (Gdy wejdziemy w issue widnieje w nim przycisk `create merge request`). Po jego stworzeniu tworzony jest osobny branch na którym możemy wykonać zadanie opisane w issue. W merge requscie, można dawać komentarze do konkretnych linii kodu co ułatwia wprowadzenie poprawek. Ważne, żebyśmy przed mergem przeanalizowali kod i dawali sobie nawzajem komentarze. Po pomyślnej weryfikacji zmian w kodzie, brancha można zmergować do maina.

### Konflikty i rebase

W przypadku większego projektu istnieje wiele branchy, kóre są rownolegle mergowane. Często dwie osoby modyfikują ten sam fragmen kodu. Istnieje wtedy konflikt i należy wykonać rebase, czyli w skrócie w kodzie pojawiają się dwie linie, jedna twoja druga z wprowadzona dopiero na maina. Trzeba je jakoś połączyć lub po prostu usunąć jedną.

W dwie osoby raczej nie będziemy potrzebować tego robić ale kto wie.

### Visual studio code

Generalnie to polecam korzystać z visuala. Można wtedy praktycznie całkowicie olać gita w formie konsolowej i wszystko wyklikać. Polecam zainstalować sobie wtyczkę `gitlens`. Pokazuje ona koło linii kodu kto ją napisał, kiedy i w jakim MR.
