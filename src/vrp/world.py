import math
import random
from typing import Optional

import numpy as np
from matplotlib import pyplot as plt

# if it is changed then whole code needs to be reviewed!!!
MAIN_STATION_ID = 0  # implementation is based on fact that it's 0


class Car:
    def __init__(self, max_capacity: int = 20) -> None:
        self.capacity = max_capacity
        self.current_capacity = max_capacity
        self.current_station_id = MAIN_STATION_ID
        self.stations_visited = [MAIN_STATION_ID]

    def can_visit(self) -> bool:
        return self.current_capacity > 0

    def visit_station(self, station: "Station"):
        if self.current_capacity > station.demand:
            self.current_capacity -= station.demand
            station.product_delivered += station.demand
            station.demand = 0
        else:
            station.product_delivered += self.current_capacity
            station.demand -= self.current_capacity
            self.current_capacity = 0
        self.stations_visited.append(station.id)

    def __str__(self) -> str:
        return f"stations visited: {self.stations_visited}"


class Station:
    def __init__(
        self, station_id: int, max_x: int = 100, max_y: int = 100, max_demand: int = 100
    ) -> None:
        self.id = station_id
        self.x = random.randint(0, max_x)
        self.y = random.randint(0, max_y)
        self.demand = random.randint(0, max_demand)
        self.product_delivered = 0
        self.full = False
        self.main_station = False

    def __str__(self) -> str:
        return f"id = {self.id}, x = {self.x}, y = {self.y}, demand = {self.demand}"


class Map:
    main_station_id: int = MAIN_STATION_ID

    def __init__(
        self,
        station_amount: int = 10,
        max_x: int = 100,
        max_y: int = 100,
        max_demand: int = 100,
    ) -> None:
        if station_amount == 0:
            self.stations = []
            return
        self.stations = [
            Station(station_id, max_x, max_y, max_demand)
            for station_id in range(station_amount)
        ]
        self.stations[0].main_station = True

    def __str__(self) -> str:
        return "\n".join([str(station) for station in self.stations])

    def plot(self, cars: Optional[list[Car]] = None) -> None:
        plt.figure(figsize=(10, 6))
        x_stations = [station.x for station in self.stations[1:]]
        y_stations = [station.y for station in self.stations[1:]]
        product_delivered = [
            str(station.product_delivered) for station in self.stations[1:]
        ]
        demands = [str(station.demand) for station in self.stations[1:]]

        """for i, station1 in enumerate(self.stations):
            for j, station2 in enumerate(self.stations):
                if i != j:
                    plt.plot(
                        [station1.x, station2.x],
                        [station1.y, station2.y],
                        color="yellow",
                        alpha=0.1,
                        zorder=2,
                    )"""

        plt.scatter(
            self.stations[0].x,
            self.stations[0].y,
            color="green",
            label="Main station",
            zorder=3,
            s=70,
        )
        plt.scatter(
            x_stations, y_stations, color="red", label="Stations", zorder=3, s=70
        )

        station_id = 1
        if cars is not None:
            for id, car in enumerate(cars):
                color = (random.random(), random.random(), random.random())
                stations_visited = car.stations_visited
                for i in range(len(stations_visited) - 1):
                    station1 = self.stations[stations_visited[i]]
                    station2 = self.stations[stations_visited[i + 1]]
                    if i == 0:
                        plt.plot(
                            [station1.x, station2.x],
                            [station1.y, station2.y],
                            color=color,
                            alpha=1,
                            zorder=2,
                            label=f"Car {id}",
                        )
                    else:
                        plt.plot(
                            [station1.x, station2.x],
                            [station1.y, station2.y],
                            color=color,
                            alpha=1,
                            zorder=2,
                        )
            for x, y, product_delivered in zip(
                x_stations, y_stations, product_delivered
            ):
                plt.text(
                    x,
                    y,
                    f"{station_id}: {product_delivered}",
                    fontsize=8,
                    color="black",
                )
                station_id += 1
        else:
            for x, y, demand in zip(x_stations, y_stations, demands):
                plt.text(x, y, f"{station_id}: {demand}", fontsize=8, color="black")
                station_id += 1

        plt.xlabel("X")
        plt.ylabel("Y")
        plt.title("Car Route")
        plt.legend()
        plt.grid(True)
        plt.tight_layout()
        plt.show()


def two_stations_distance(station_a: Station, station_b: Station) -> float:
    x = (station_a.x - station_b.x) ** 2
    y = (station_a.y - station_b.y) ** 2
    distance = math.sqrt(x + y)
    return float(np.round(distance, decimals=4))
