import argparse
import json
import sys
from pathlib import Path
from typing import Any

from vrp.world import Map, Station


def dataset_file_to_dict(dataset_path: Path) -> dict[str, Any]:
    dataset_data = {
        "dimension": 0,
        "capacity": 0,
        "nodes_coords": [],
        "nodes_demands": [],
    }
    coords_flag = False
    demand_flag = False
    with open(dataset_path, "r", encoding="utf-8") as file:
        for line in file:
            if line[:9] == "DIMENSION":
                elements = line.split()
                dataset_data["dimension"] = int(elements[2])
            if line[:8] == "CAPACITY":
                elements = line.split()
                dataset_data["capacity"] = int(elements[2])
            if line[:13] == "DEPOT_SECTION":
                demand_flag = False
            if demand_flag:
                elements = line.split()
                dataset_data["nodes_demands"].append(elements[1])
            if line[:14] == "DEMAND_SECTION":
                coords_flag = False
                demand_flag = True
            if coords_flag:
                elements = line.split()
                dataset_data["nodes_coords"].append([elements[1], elements[2]])
            if line[:18] == "NODE_COORD_SECTION":
                coords_flag = True
    return dataset_data


def dataset_dict_to_map(map_dict: dict[str, Any]) -> Map:
    new_map = Map(station_amount=0)
    id = 0
    for station_coords, station_demand in zip(
        map_dict["nodes_coords"], map_dict["nodes_demands"]
    ):
        new_station = Station(station_id=id)
        new_station.x = float(station_coords[0])
        new_station.y = float(station_coords[1])
        new_station.demand = float(station_demand)
        if station_demand == 0:
            new_station.main_station = True
        new_map.stations.append(new_station)
        id += 1
    return new_map


def parse_args(argv):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("dataset_path", type=Path, help="")

    return parser.parse_args(argv[1:])


def main(argv=None) -> None:
    args = parse_args(argv or sys.argv)
    datasets_path = Path(args.dataset_path)

    datatests_paths_list = list(datasets_path.rglob("*.vrp"))
    datatests_paths_list.sort()

    map_dict_list = {}
    maps = []

    for dataset_path in datatests_paths_list:
        map_dict = dataset_file_to_dict(dataset_path)
        map_dict_list[dataset_path.name] = map_dict
        maps.append(dataset_dict_to_map(map_dict))

    for map in maps:
        print(map)
        # map.plot()

    with open(dataset_path.parent / "dataset.vrp.json", "w", encoding="utf-8") as file:
        json.dump(map_dict_list, file, indent=4)


if __name__ == "__main__":
    main()
