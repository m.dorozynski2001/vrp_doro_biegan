import argparse
import random
import sys
from pathlib import Path

import matplotlib.pyplot as plt
from tqdm import tqdm

from vrp.vrp_datasets_parser import dataset_dict_to_map, dataset_file_to_dict
from vrp.world import Car, Map, two_stations_distance

UNFULFILLED_PENALTY = 10
UNUSED_PENALTY = 1
NOT_VISITED_PENALTY = 100


class GeneticAlgorithm:

    def __init__(
        self,
        population_size: int = 50,
        generations: int = 100,
        mutation_rate: float = 0.1,
        num_cars: int = 1,
        car_capacity: int = 20,
    ) -> None:
        self.population_size = population_size
        self.generations = generations
        self.mutation_rate = mutation_rate
        self.num_cars = num_cars
        self.car_capacity = car_capacity

    def initialize_population(self, map_obj: Map) -> list[list[list[int]]]:
        population = []
        station_ids = list(range(1, len(map_obj.stations)))

        for _ in range(self.population_size):
            remaining_stations = station_ids[:]
            cars_routes = [[] for _ in range(self.num_cars)]

            for car_route in cars_routes:
                car_route.append(0)  # Start at the main station

            while remaining_stations:
                for car_route in cars_routes:
                    if not remaining_stations:
                        break
                    station_id = random.choice(remaining_stations)
                    car_route.append(station_id)
                    remaining_stations.remove(station_id)

            for car_route in cars_routes:
                car_route.append(0)  # End at the main station

            population.append(cars_routes)

        return population

    # objective function
    def calculate_fitness(self, cars_routes: list[list[int]], map_obj: Map) -> float:
        total_distance = 0.0
        total_unfulfilled_demand = 0  # penalize unfulfilled demand
        total_unused_capacity = 0  # penalize unused capcity from the cars
        total_not_visited = 0
        visited_stations = [False] * len(map_obj.stations)  # all initlialy unvisited

        for route in cars_routes:
            current_capacity = self.car_capacity
            for i in range(len(route) - 1):
                station_a = map_obj.stations[route[i]]
                station_b = map_obj.stations[route[i + 1]]
                total_distance += two_stations_distance(station_a, station_b)

                # mark visited
                if not visited_stations[route[i]]:
                    visited_stations[route[i]] = True
                if not visited_stations[route[i + 1]]:
                    visited_stations[route[i + 1]] = True

                if station_a.id != 0:  # Skip main station
                    if current_capacity >= station_a.demand:
                        current_capacity -= station_a.demand
                    else:
                        total_unfulfilled_demand += station_a.demand - current_capacity
                        current_capacity = 0
            if current_capacity > 0:
                total_unused_capacity += current_capacity

            # penalize not visited stations
            for i, visited in enumerate(visited_stations):
                if not visited:
                    total_not_visited += NOT_VISITED_PENALTY

        fitness = 1 / (
            total_distance
            + total_not_visited
            + total_unfulfilled_demand * UNFULFILLED_PENALTY
            + total_unused_capacity * UNUSED_PENALTY
        )
        return fitness if fitness > 0 else float("inf")

    def crossover(
        self, parent1: list[list[int]], parent2: list[list[int]]
    ) -> list[list[int]]:
        child = []
        for route_p1, route_p2 in zip(parent1, parent2):
            # choose shorter list for insert, it guarantees that we wont go out of lists scope
            route1 = min([route_p1, route_p2], key=len, default=route_p1)
            route2 = max([route_p1, route_p2], key=len, default=route_p2)
            size = len(route1)
            if size > 3:
                start, end = sorted(random.sample(range(1, size - 1), 2))
            else:
                start, end = (1, 1)
            child_route = [-1] * size
            child_route[0] = 0
            child_route[-1] = 0
            child_route[start:end] = route1[start:end]
            remaining_index = 0
            for i in range(size):
                if child_route[i] == -1:
                    while route2[remaining_index] in child_route:
                        remaining_index += 1
                    child_route[i] = route2[remaining_index]
                    remaining_index += 1
            child.append(child_route)
        return child

    def mutate(self, cars_routes: list[list[int]]) -> list[list[int]]:
        for route in cars_routes:
            if random.random() < self.mutation_rate:
                size = len(route)
                if size > 3:
                    idx1, idx2 = sorted(random.sample(range(1, size - 1), 2))
                else:
                    idx1, idx2 = (1, 1)
                route[idx1], route[idx2] = route[idx2], route[idx1]
        return cars_routes

    def evolve(self, map_obj: Map) -> list[list[int]]:
        population = self.initialize_population(map_obj)
        objective_func_val = []

        for _ in tqdm(range(self.generations)):
            fitness_scores = [
                self.calculate_fitness(cars_routes, map_obj)
                for cars_routes in population
            ]
            sorted_population = [
                x for _, x in sorted(zip(fitness_scores, population), reverse=True)
            ]
            selected_parents = sorted_population[: self.population_size // 2]

            next_population = selected_parents[:]
            while len(next_population) < self.population_size:
                parent1, parent2 = random.choices(selected_parents, k=2)
                child = self.crossover(parent1, parent2)
                child = self.mutate(child)
                next_population.append(child)

            population = next_population

            # Record the best fitness value in this iteration
            best_fitness = max(fitness_scores)
            objective_func_val.append(1 / best_fitness)

        best_cars_routes = max(
            population,
            key=lambda cars_routes: self.calculate_fitness(cars_routes, map_obj),
        )

        self.objective_func_val = objective_func_val

        return best_cars_routes

    def plot_objective_func(self):
        plt.plot(self.objective_func_val)
        plt.xlabel("Generations")
        plt.ylabel("Objective function")
        plt.title("Objective function value Over Generations")
        plt.show()


def parse_args(argv):
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("--vrp-file", type=Path)
    parser.add_argument("--population_size", default=200, type=int, help="")
    parser.add_argument("--generations", default=400, type=int, help="")
    parser.add_argument("--mutation_rate", default=0.1, type=float, help="")
    return parser.parse_args(argv[1:])


def main(argv=None):
    args = parse_args(argv or sys.argv)
    dataset_path = args.vrp_file
    population_size = args.population_size
    generations = args.generations
    mutation_rate = args.mutation_rate

    map_dict = dataset_file_to_dict(dataset_path)
    map_instance = dataset_dict_to_map(map_dict)
    map_instance.plot()

    station_amount = map_dict["dimension"]
    car_capacity = map_dict["capacity"]

    total_demand = 0
    for station in map_instance.stations:
        total_demand += station.demand

    num_cars = int(total_demand / car_capacity) + 1
    print(f"{num_cars}, {total_demand}, {car_capacity}")

    stations_visited = {}
    Wrong_routes = True
    while Wrong_routes:
        genetic_algorithm = GeneticAlgorithm(
            population_size=population_size,
            generations=generations,
            mutation_rate=mutation_rate,
            num_cars=num_cars,
            car_capacity=car_capacity,
        )
        best_routes = genetic_algorithm.evolve(map_instance)
        print("Best routes found:")
        for id, route in enumerate(best_routes):
            print(f"car {id}: {route}")

        cars = [Car(car_capacity) for _ in range(num_cars)]
        for car, route in zip(cars, best_routes):
            for station_id in route:
                station = map_instance.stations[station_id]
                if car.can_visit():
                    car.visit_station(station)
                else:
                    break
            car.stations_visited.append(0)  # Return to main station

        for car in cars:
            for station in car.stations_visited:
                stations_visited[station] = True

        if len(stations_visited) != station_amount:
            num_cars += 1
        else:
            Wrong_routes = False

    genetic_algorithm.plot_objective_func()
    map_instance.plot(cars)


# Usage example:
if __name__ == "__main__":
    main()
