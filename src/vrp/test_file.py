from vrp.world import MAIN_STATION_ID, Car, Map, two_stations_distance

map = Map()

for i in range(10):
    for j in range(10):
        print(
            f"distance from station {i} to station {j}: {two_stations_distance(map.stations[i], map.stations[j])}"
        )
    print("")

print(map)


car = Car()

while True:
    next_station_id = 0
    next_min_distance: float = 100000
    for id, station in enumerate(map.stations):
        distance = two_stations_distance(map.stations[car.current_station_id], station)
        print(f"{next_min_distance}, {distance}")
        if (
            distance < next_min_distance
            and not station.full
            and distance != 0
            and not station.main_station
        ):
            next_min_distance = distance
            next_station_id = id
    car.stations_visited.append(next_station_id)
    car.current_station_id = next_station_id
    map.stations[next_station_id].full = True
    if next_station_id == MAIN_STATION_ID:
        break

print(car.stations_visited)
map.plot()
map.plot([car])
